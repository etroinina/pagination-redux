import React from 'react';
import './App.css';
import { store } from './store';

store.subscribe(function () {
    console.log('store has been updated');
    console.log(store.getState());
});

export const App = () => (
    <div className="App">
      Lorem ipsum dolor
    </div>
);