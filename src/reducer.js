const initialState = {
    name: 'Vasya'
};

export const reducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch(type) {
        case 'CHANGE_NAME':
            return { ...state, name: payload };
        default:
            return state;
    }
};

