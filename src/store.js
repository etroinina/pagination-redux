import { createStore } from 'redux';
import { reducer } from './reducer';

export const store = createStore(reducer);


setTimeout(() => {
    store.dispatch({ type: 'SOME_ACTION', payload: 'blablabla'});
}, 1500);